//
//  NewsListViewModel.swift
//  NewsReader
//
//  Created by Luka Šarčević on 13/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import Foundation

protocol NewsListModelDelegate: class {
    func didFetch(with result: NewsListFetchResult)
    func didCatch(error: Error)
}

enum ListDataType {
    case topNews
    case everyNews

    var caseURL: String {
        switch self {
        case .everyNews:
            return APIEndpoints.everyNews
        case .topNews:
            return APIEndpoints.topNews
        }
    }
}

enum NewsListFetchResult {
    case initial
    case pagination([IndexPath])
}

final class NewsListViewModel {

    // MARK: Variables
    private weak var delegate: NewsListModelDelegate!
    var searchText: String = ""
    private var currentPage: Int {
        if newsDataSource?.articles.count ?? 0 < newsDataSource?.totalResults ?? 0 {
            let result = Double((newsDataSource?.articles.count ?? 0)/20)
            let roundResult = result.rounded(.awayFromZero)
            return Int(roundResult)
        } else {
            return 0
        }

    }
    // This is main data source for view controller
    var newsDataSource: NewsResponse? {
        if searchText.isEmpty {
            switch tableDataType {
            case .everyNews:
                return allNewsList
            case .topNews:
                return topNewsList
            }
        } else {
            return searchNewsList
        }
    }
    // These three are stored, and they get called with newsDataSource depenging on a case
    private var topNewsList: NewsResponse?
    private var allNewsList: NewsResponse?
    private var searchNewsList: NewsResponse?
    private var isFetchInProgress = false
    var tableDataType: ListDataType = .topNews {
        didSet {
            currentTask?.cancel()
            if allNewsList == nil {
                fetchData()
            }
        }
    }
    private var currentTask: URLSessionDataTask?

    // MARK: - Init
    init(delegate: NewsListModelDelegate) {
        self.delegate = delegate
        fetchData()
    }

    // MARK: - Network requests
    func refreshData() {
        allNewsList = nil
        searchNewsList = nil
        topNewsList = nil
        fetchData()
    }

    func fetchData() {
        guard !isFetchInProgress else { return }
        isFetchInProgress = true
        currentTask = NetworkHelper.fetchNewsData(paramethers: getParams(),
                                                  newsULR: tableDataType.caseURL,
                                                  completion: { [weak self] (news) in
                                                    guard let self = self else { return }
                                                    self.isFetchInProgress = false
                                                    guard let news = news else { self.delegate.didFetch(with: .initial); return }
                                                    // There are 3 NewsResponse objects, each used for one filter
                                                    // 1. Search is active
                                                    if !self.searchText.isEmpty {
                                                        if self.currentPage == 0 || self.searchNewsList?.totalResults != news.totalResults || news.totalResults == 0 {
                                                            self.searchNewsList = news
                                                            self.delegate.didFetch(with: .initial)
                                                        } else {
                                                            self.searchNewsList?.articles += news.articles
                                                            let paths = self.calculateIndexPathsToReload(from: news.articles)
                                                            self.delegate.didFetch(with: .pagination(paths))
                                                        }
                                                    } else {
                                                        // 2. & 3. Depends what is selected on segmented control
                                                        switch self.tableDataType {
                                                        case .everyNews:
                                                            if self.currentPage == 0 {
                                                                self.allNewsList = news
                                                                self.delegate.didFetch(with: .initial)
                                                            } else {
                                                                self.allNewsList?.articles += news.articles
                                                                let paths = self.calculateIndexPathsToReload(from: news.articles)
                                                                self.delegate.didFetch(with: .pagination(paths))
                                                            }
                                                        case .topNews:

                                                            if self.currentPage == 0 {
                                                                self.topNewsList = news
                                                                self.delegate.didFetch(with: .initial)
                                                            } else {
                                                                self.topNewsList?.articles += news.articles
                                                                let paths = self.calculateIndexPathsToReload(from: news.articles)
                                                                self.delegate.didFetch(with: .pagination(paths))
                                                            }
                                                        }
                                                    }
            }, failure: { [weak self] (error) in
                self?.isFetchInProgress = false
                self?.delegate.didCatch(error: error)
        })
    }

    func getParams() -> [String: String] {
        let parameters: [String: String]
        switch tableDataType {
        case .everyNews:
            if searchText != ""{
                parameters = ["apiKey": "154e0ef76b4e438fac1acdce32fdfc6f", "sortBy": "relevance", "q": searchText, "page": "\(currentPage+1)"]
            } else {
                // Either search text category or domain are mandatory as params - so here bbc.co.uk is picked as domain
                parameters = ["apiKey": "154e0ef76b4e438fac1acdce32fdfc6f", "sortBy": "publishedAt", "domains": "bbc.co.uk", "page": "\(currentPage+1)"]
            }

        case .topNews:
            if searchText != ""{
                parameters = ["apiKey": "154e0ef76b4e438fac1acdce32fdfc6f", "sortBy": "relevance", "q": searchText, "page": "\(currentPage+1)"]
            } else {
                // Either search text, country, category or source are mandatory as params - so here GB is picked as country
                 parameters = ["apiKey": "154e0ef76b4e438fac1acdce32fdfc6f", "sortBy": "publishedAt", "country": "gb", "page": "\(currentPage+1)"]
            }
        }
        return parameters
    }

    private func calculateIndexPathsToReload(from newNews: [NewsModel]) -> [IndexPath] {
        let startIndex = (newsDataSource?.articles.count ?? newNews.count) - newNews.count
        let endIndex = startIndex + newNews.count
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }

}
