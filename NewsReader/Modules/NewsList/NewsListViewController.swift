//
//  NewsListViewController.swift
//  NewsReader
//
//  Created by Luka Šarčević on 13/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import UIKit

final class NewsListViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var segmentedControl: NLSegmentedControl!
    @IBOutlet private weak var searchBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var searchBar: UISearchBar!

    // MARK: Property
    private var viewModel: NewsListViewModel!
    private weak var timer: Timer?

    // MARK: Variables
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self,
                                 action: #selector(refreshData),
                                 for: UIControl.Event.valueChanged)
        return refreshControl
    }()

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        segmentedControl.setTitle(L10n.topHead, forSegmentAt: 0)
        segmentedControl.setTitle(L10n.allNews, forSegmentAt: 1)
        hideKeyboardWhenTappedAround()
        viewModel = NewsListViewModel(delegate: self)
        title = L10n.newsListTitle
        tableView.register(nibWithCellClass: NewsListTableViewCell.self)
        tableView.addSubview(refreshControl)
        tableView.tableFooterView = UIView()
    }

    // MARK: Actions
    @IBAction private func segmentedControlAction(_ sender: NLSegmentedControl) {
        searchBar.text = ""
        viewModel.searchText = ""
        switch sender.selectedSegmentIndex {
        case 0:
            viewModel.tableDataType = .topNews
            tableView.reloadData()
            return
        default:
            viewModel.tableDataType = .everyNews
            tableView.reloadData()
            return
        }
    }

    @objc func fetchData() {
        viewModel.fetchData()
    }

    @objc func refreshData() {
        viewModel.refreshData()
    }
}

// MARK: - ViewModel delegate
extension NewsListViewController: NewsListModelDelegate {

    func didFetch(with result: NewsListFetchResult) {

        onMainThread {
            if self.tableView.delegate == nil {
                self.tableView.delegate = self
                self.tableView.dataSource = self
            }
                switch result {
                case .initial:
                    if self.viewModel.newsDataSource?.totalResults == 0 {
                        self.tableView.setEmptyMessage(L10n.noResults)
                    }
                    self.tableView.reloadData()
                case .pagination(let paths):
                    self.tableView.beginUpdates()
                    self.tableView.insertRows(at: paths, with: .fade)
                    self.tableView.endUpdates()
                }
                self.refreshControl.endRefreshing()
        }
    }

    func didCatch(error: Error) {
        onMainThread {
            self.refreshControl.endRefreshing()
        }
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (_) in
            self.fetchData()
        }))
        present(alert, animated: true)
    }
}

// MARK: - SearchBar
extension NewsListViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.searchText = searchText
        // Cancel any previous timer
        timer?.invalidate()
        if searchText != "" {
            // …schedule a timer for 0.5 seconds
            timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (timer) in
                self.fetchData()
                timer.invalidate()
            })
        } else {
            tableView.reloadData()
            fetchData()
            dismissKeyboard()
        }
    }
}

// MARK: - TableView
extension NewsListViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numberOfArticles = viewModel.newsDataSource?.articles.count else {
            return 0
        }
        return numberOfArticles
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newsVC: NewsDetailsViewController = NewsDetailsViewController(nibName: "NewsDetailsViewController", bundle: nil)
        newsVC.viewModel = viewModel.newsDataSource?.articles[indexPath.row]
        navigationController?.pushViewController(newsVC, animated: true)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: NewsListTableViewCell.self, for: indexPath)
        if let article = viewModel.newsDataSource?.articles[indexPath.row] {
            cell.configure(with: article)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard
            let maxArticlesCount = viewModel.newsDataSource?.totalResults,
            let currentArticlesCount = viewModel.newsDataSource?.articles.count else {
                return
        }
        if indexPath.row == (currentArticlesCount - 1) && currentArticlesCount < maxArticlesCount {
            fetchData()
        }
    }
}

// MARK: - ScrollView
extension NewsListViewController: UIScrollViewDelegate {

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        dismissKeyboard()
        if velocity.y > 0 {
            //Code will work without the animation block.I am using animation block in case if you want to set any delay to it.
            searchBarHeightConstraint.constant = 0
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions(), animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        } else {
            searchBarHeightConstraint.constant = 50
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions(), animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
}
