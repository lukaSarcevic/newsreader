//
//  NewsListTableViewCell.swift
//  NewsReader
//
//  Created by Luka Šarčević on 13/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import UIKit
import SDWebImage

final class NewsListTableViewCell: NRTableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeCounterLabel: UILabel!
    @IBOutlet weak var timeImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(with news: NewsModel?) {

        guard let newObj = news else {
            return
        }
        if let stringURL = newObj.urlToImage, let imageURL = URL(string: stringURL) {
            thumbnailImageView?.sd_setImage(with: imageURL,
                                                 placeholderImage: Asset.placeholderIcon.image,
                                                 options: .allowInvalidSSLCertificates
            )
            titleLabel.text = newObj.title
            if let publishedAtString = newObj.publishedAt,
                let timePublished = Date(iso8601String: publishedAtString)?.relativelyFormatted {
                timeCounterLabel.text = timePublished
                thumbnailImageView.isHidden = false
            } else {
                thumbnailImageView.isHidden = true
            }
        }
    }
}
