//
//  NewsDetailsViewController.swift
//  NewsReader
//
//  Created by Luka Šarčević on 14/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import UIKit

final class NewsDetailsViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet private weak var newsImageView: UIImageView!
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var readMoreButton: UIButton!

    // MARK: Property
    // Here we dont have regular MVVM model, since pushed model has all of data we need
    // There are no action that need to be handled inside model
    var viewModel: NewsModel!

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViewController()
        title = L10n.newsListTitle
    }

    private func setUpViewController() {
        if let stringURL = viewModel.urlToImage, let imageURL = URL(string: stringURL) {
            newsImageView?.sd_setImage(with: imageURL,
                                            placeholderImage: Asset.placeholderIcon.image,
                                            options: .allowInvalidSSLCertificates)
        }
        authorLabel.text = viewModel.author
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        readMoreButton.setTitle(L10n.readOnline, for: .normal)
    }

    // MARK: Actions
    @IBAction private func readMoreAction(_ sender: UIButton) {
        let alert = UIAlertController(title: L10n.warning, message: L10n.appLeaveWarning, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: L10n.cancel, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: L10n.ok, style: .default, handler: { (_) in
            guard let url = URL(string: self.viewModel.pageUrl ?? "www.google.com") else {
                return
            }
            UIApplication.shared.open(url)
        }))
        present(alert, animated: true)
    }
}
