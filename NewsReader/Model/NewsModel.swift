//
//  NewsModel.swift
//  NewsReader
//
//  Created by Luka Šarčević on 13/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import Foundation

struct NewsResponse: Codable {
    // Model properties
    var totalResults: Int
    var articles: [NewsModel]
    let status: String
}

struct NewsModel: Codable {
    // Model properties
    let source: Source?
    let author: String?
    let title: String?
    let description: String?
    let pageUrl: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?

    private enum CodingKeys: String, CodingKey {
        case source
        case author
        case title
        case description
        case pageUrl = "url"
        case urlToImage
        case publishedAt
        case content
    }
}

struct Source: Codable {
    let id: String?
    let name: String?
}
