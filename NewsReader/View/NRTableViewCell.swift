//
//  NRTableViewCell.swift
//  NewsReader
//
//  Created by Luka Šarčević on 15/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import UIKit

class NRTableViewCell: UITableViewCell {

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}
