//
//  NLSegmentedControl.swift
//  NewsReader
//
//  Created by Luka Šarčević on 14/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import UIKit

/// UISegmentedControl with custom Fonts / color's
class NLSegmentedControl: UISegmentedControl {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    private func configure() {
        // Add lines below the segmented control's tintColor
        self.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont(name: "ArialMT", size: 16)!,
            NSAttributedString.Key.foregroundColor: UIColor.white
            ], for: .normal)

        self.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont(name: "Arial-BoldMT", size: 18)!,
            NSAttributedString.Key.foregroundColor: UIColor.white
            ], for: .selected)

        self.backgroundColor = .lightGray
        self.tintColor = .black
    }
}
