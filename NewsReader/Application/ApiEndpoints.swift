//
//  ApiEndpoints.swift
//  NewsReader
//
//  Created by Luka Šarčević on 13/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import UIKit
/// Contains base URL for API and images
struct APIEndpoints {

    // MARK: - News API
    /// Base URL for news fetching
    static let newsBaseURL = "https://newsapi.org/v2"

    // This endpoint provides live top and breaking headlines for a country, specific category in a country, single source, or multiple sources
    static let topNews = newsBaseURL + "/top-headlines?"

    // This endpoint suits article discovery and analysis, but can be used to retrieve articles for display, too.
    static let everyNews = newsBaseURL + "/everything?"
}
