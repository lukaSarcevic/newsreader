//
//  NRExtensions.swift
//  NewsReader
//
//  Created by Luka Šarčević on 13/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import UIKit

// NOTE: Because this is small application, all of there extensions are cramed in the same file
// Otherwise each extension would be in separated file

// MARK: - UIViewController
extension UIViewController {

    // This extension could also be put inside some BaseView controller if this was app with multuple screens
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - UITableView
extension UITableView {

    /// Functions below are used to show message when tableView is empty
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "ArialMT", size: 20)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }

    ///  Assumes that the .xib filename and cell class has the same name.
    /// - Parameters:
    ///   - name: UITableViewCell type.
    ///   - bundleClass: Class in which the Bundle instance will be based on.
    public func register<T: UITableViewCell>(nibWithCellClass name: T.Type, at bundleClass: AnyClass? = nil) {
        let identifier = String(describing: name)
        var bundle: Bundle?

        if let bundleName = bundleClass {
            bundle = Bundle(for: bundleName)
        }

        register(UINib(nibName: identifier, bundle: bundle), forCellReuseIdentifier: identifier)
    }

    /// - Parameters:
    ///   - name: UITableViewCell type.
    ///   - indexPath: location of cell in tableView.
    /// - Returns: UITableViewCell object with associated class name.
    public func dequeueReusableCell<T: UITableViewCell>(withClass name: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: name), for: indexPath) as? T else {
            fatalError("Couldn't find UITableViewCell for \(String(describing: name))")
        }
        return cell
    }
}

// MARK: - Threading extension
/// Shortened way of calling specific threads
func onMainThread(_ block: @escaping (() -> Void)) {
    DispatchQueue.main.async(execute: block)
}

// MARK: - Date extension
public extension Date {

    func languageIsCro() -> Bool {
        guard let deviceLanguage = Locale.current.languageCode else {
            return false
        }
        return deviceLanguage.contains("hr")
    }

    // This could also be put in separate model in case it would repeat on multiple cells
    var relativelyFormatted: String {

        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.allowedUnits = [.year, .day, .hour, .minute, .second]
        let currentTime = Date()
        let components = Calendar.current.dateComponents([.year, .month, .weekOfYear, .day, .hour, .minute, .second], from: self, to: currentTime)

        if let years = components.year, years > 0 {
            if languageIsCro() {
                return "Prije \(years) godin\(years < 5 ? "e" : "a")"
            } else {
                return "\(years) year\(years == 1 ? "" : "s") ago"
            }
        }
        if let months = components.month, months > 0 {
            if languageIsCro() {
                return "Prije \(months) mjesec\(months < 5 ? "a" : "i")"
            } else {
                return "\(months) month\(months == 1 ? "" : "s") ago"
            }
        }

        if let weeks = components.weekOfYear, weeks > 0 {
            if languageIsCro() {
                return "Prije \(weeks) tjed\(weeks < 5 ? "na" : "ana")"
            } else {
                return "\(weeks) week\(weeks == 1 ? "" : "s") ago"
            }
        }
        if let days = components.day, days > 0 {
            if languageIsCro() {
                guard days > 1 else { return "Jučer" }
                return "Prije \(days) dana)"
            } else {
                guard days > 1 else { return "yesterday" }
                return "\(days) day\(days == 1 ? "" : "s") ago"
            }
        }

        if let hours = components.hour, hours > 0 {
            if languageIsCro() {
                return "Prije \(hours) sat\(hours < 5 ? "a" : "i")"
            } else {
                return "\(hours) hour\(hours == 1 ? "" : "s") ago"
            }
        }

        if let minutes = components.minute, minutes > 0 {
            if languageIsCro() {
                return "Prije \(minutes) minut\(minutes < 5 ? "e" : "a")"
            } else {

                return "\(minutes) minute\(minutes == 1 ? "" : "s") ago"
            }
        }

        if let seconds = components.second, seconds > 30 {
            if languageIsCro() {
                return "Prije \(seconds) sekund\(seconds < 5 ? "e" : "i")"
            } else {
                return "\(seconds) second\(seconds == 1 ? "" : "s") ago"
            }
        }
        if languageIsCro() {
            return "Upravo!"

        } else {

            return "just now"
        }
    }

    ///     let date = Date(iso8601String: ""2019-05-13T23:00:07Z"")
    ///
    /// - Parameter iso8601String: ISO8601 string of format (yyyy-MM-dd'T'HH:mm:ssZ).
    init?(iso8601String: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let date = dateFormatter.date(from: iso8601String) {
            self = date
        } else {
            return nil
        }
    }
}
