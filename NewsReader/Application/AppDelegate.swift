//
//  AppDelegate.swift
//  NewsReader
//
//  Created by Luka Šarčević on 13/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setLanguage()
        setBackButton()
        setRootViewController()
        return true
    }

    // English is default language, unless device is on Croatian
    func setLanguage() {
        guard let deviceLanguage = Locale.current.languageCode else {
            return
        }
        if deviceLanguage.contains("hr") {
            UserDefaults.standard.set("hr", forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
        } else {
            UserDefaults.standard.set("en", forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
        }
    }

    // Sets custom back navigation image
    func setBackButton() {
        let backImage = Asset.navBack.image
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage

        // Removes title from back navigation button
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
    }

    // Opens first screen from xib
    func setRootViewController() {
        window = UIWindow(frame: UIScreen.main.bounds)
        let navController = UINavigationController(rootViewController: NewsListViewController())
        navController.navigationBar.barTintColor = UIColor.black
        navController.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white,
                                                           .font: UIFont(name: "Arial-BoldMT", size: 24)!]
        navController.navigationBar.isTranslucent = false
        navController.navigationBar.isOpaque = true
        window!.rootViewController = navController
        window!.makeKeyAndVisible()
    }
}
