//
//  NetworkHelper.swift
//  NewsReader
//
//  Created by Luka Šarčević on 13/05/2019.
//  Copyright © 2019 Luka Šarčević. All rights reserved.
//

import Foundation

enum NetworkError: Error {

    case invalidURL

    var localizedDescription: String {
        switch self {
        case .invalidURL:
            return "Invalid URL, please check your URL and try again."
        }
    }
}

// Note: I usualy use Alamofire for this, but since its only one API call -> native is used
struct NetworkHelper {

    /// API call for fetching NewsModel's
    ///
    /// - Parameters:
    ///   - pageCount: deafult set to 1, but increases incrementaly
    ///   - completion: completion block ([NewsModel]?) -> Void
    ///   - failure: failure block ((Error) -> Void)
    static func fetchNewsData(paramethers: [String: String],
                              newsULR: String,
                              completion: @escaping(NewsResponse?) -> Void,
                              failure: @escaping ((Error) -> Void)) -> URLSessionDataTask? {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 10.0
        sessionConfig.timeoutIntervalForResource = 20.0
        let session = URLSession(configuration: sessionConfig)

        var compoments = URLComponents(string: newsULR)
        compoments?.queryItems = paramethers.map {(parKey, value) in
            URLQueryItem(name: parKey, value: value)
        }

        guard let url = compoments?.url else {
            failure(NetworkError.invalidURL)
            return nil
        }
        let request = URLRequest(url: url)

        let task = session.dataTask(with: request) {(data, response, error) in

            guard let httpResponse = response as? HTTPURLResponse else {
                guard let failMes = error else { return }
                failure(failMes)
                return
            }
            if httpResponse.statusCode >= 200 && httpResponse.statusCode < 400 {
                guard let data = data else { return }
                let decoder = JSONDecoder()
                let dataResponse = try? decoder.decode(NewsResponse.self, from: data)
                completion(dataResponse)
            } else {
                guard let error = error else { return }
                failure(error)
            }
        }
        task.resume()
        return task
    }
}
