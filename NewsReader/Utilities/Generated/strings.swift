// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {
  /// All news
  internal static let allNews = L10n.tr("Localizable", "allNews")
  /// You will leave this app
  internal static let appLeaveWarning = L10n.tr("Localizable", "appLeaveWarning")
  /// Cancel
  internal static let cancel = L10n.tr("Localizable", "cancel")
  /// News
  internal static let newsListTitle = L10n.tr("Localizable", "newsListTitle")
  /// There are no news matching these criteria
  internal static let noResults = L10n.tr("Localizable", "noResults")
  /// Ok
  internal static let ok = L10n.tr("Localizable", "ok")
  /// Read more about it on original text
  internal static let readOnline = L10n.tr("Localizable", "readOnline")
  /// Top headlines
  internal static let topHead = L10n.tr("Localizable", "topHead")
  /// Warning
  internal static let warning = L10n.tr("Localizable", "warning")
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
